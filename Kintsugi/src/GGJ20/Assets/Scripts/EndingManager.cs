﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndingManager : MonoBehaviour
{
    public AudioClip titleAudio;
    public AudioClip fanfare;

    public GameObject textToFade;
    public int secondsToFade;

    private float timer;
    private bool titleDone = false;


    // Start is called before the first frame update
    void Start()
    {
        timer = Time.time + 1;
        gameObject.GetComponent<AudioSource>().PlayOneShot(titleAudio);
    }

    // Update is called once per frame
    void Update()
    {
        //if (!titleDone)
        //{
        //    gameObject.GetComponent<AudioSource>().PlayOneShot(titleAudio);
        //    titleDone = true;
        //}

        if(Time.time > timer && !titleDone)
        {
            gameObject.GetComponent<AudioSource>().PlayOneShot(fanfare);
            titleDone = true;
        }

        //if (titleDone )
        //{

        //}

        if (Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene(1);
        }
    }
}
