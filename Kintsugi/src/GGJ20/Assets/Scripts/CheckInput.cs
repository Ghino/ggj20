﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CheckInput : MonoBehaviour
{

    ///ERROR ON COMMIT
    ///Function del singolo IF
    ///(OK)->Prendere in input la freccia premuta
    ///(OK)->Inserirlo in un array per eseguire calcoli di statistica
    ///(OK)->Aumentare il puntatore -> Se è arrivato al limite uscire dalla funzione
    ///(OK)->Prelevare il GameObject delle freccie -> confrontare se è uguale al valore inserito

    //[SerializeField] GameObject[] arrayRow;
    [SerializeField] GameObject arrowMngScript;
    [SerializeField] GameObject GameAudio;
    [SerializeField] bool newSet;
    

    public GameObject remainingSetText;
    public GameObject cdText;
    public float timerSinglePress = 1f;
    private float cdTimer;
    private float timer;

    //[SerializeField] GameObject PS; ///To implement, not mandatory.
    public List<string> finalArrow;
    int posLettura = 0;
    int LimitArray;

    [SerializeField] int ROUNDTOEND;
    private int roundNumber = 0;

    //conteggio punti
    public float p1Points = 0;
    public float p2Points = 0;

    //private void Awake()
    //{
    //    timer = Time.time + 0.02f;
    //}

    //Check a ogni frame dell'input inserito.
    public void Update()
    {
        cdTimer -= Time.deltaTime;
        cdTimer = Mathf.Round(cdTimer * 100) / 100;

        if(cdTimer <= 0)
        {
            cdTimer = timerSinglePress;
            //

            //

            //
            arrowMngScript.GetComponent<ArrowMngrScript>().SwitchBG(false, posLettura);
            posLettura++;
            p1Points -= 1;
        }

        if (Time.time >= timer)
        {
            cdText.GetComponent<TMPro.TextMeshProUGUI>().text = cdTimer.ToString();
            timer += 0.02f;
        }

        if (Input.GetKeyDown("up"))
        {
            //Debug.Log("Up pressed");
            finalArrow.Add("up");
            //string toCheck = arrayRow[posLettura].GetComponent<ArrowScript>().GetValue(); //Use GetFunction to get value of arrow. 
            ///Ottenere valore della freccia nella posizione posLettura
            string toCheck = arrowMngScript.GetComponent<ArrowMngrScript>().GetObj(posLettura);
            CheckValue(toCheck, "up");
            posLettura++;
        }
        else if (Input.GetKeyDown("down")){
            //Debug.Log("Down pressed");
            finalArrow.Add("down");
            //string toCheck = arrayRow[posLettura].GetComponent<ArrowScript>().GetValue(); //Use GetFunction to get value of arrow. 
            //CheckValue(toCheck, "down");
            string toCheck = arrowMngScript.GetComponent<ArrowMngrScript>().GetObj(posLettura);
            CheckValue(toCheck, "down");
            posLettura++;
        }
        else if (Input.GetKeyDown("left")){
            //Debug.Log("Left pressed");
            finalArrow.Add("left");
            //string toCheck = arrayRow[posLettura].GetComponent<ArrowScript>().GetValue(); //Use GetFunction to get value of arrow. 
            //CheckValue(toCheck, "left");
            string toCheck = arrowMngScript.GetComponent<ArrowMngrScript>().GetObj(posLettura);
            CheckValue(toCheck, "left");
            posLettura++;
        }
        else if (Input.GetKeyDown("right")){
            //Debug.Log("Right pressed");
            finalArrow.Add("right");
            //string toCheck = arrayRow[posLettura].GetComponent<ArrowScript>().GetValue(); //Use GetFunction to get value of arrow. 
            //CheckValue(toCheck, "right");
            string toCheck = arrowMngScript.GetComponent<ArrowMngrScript>().GetObj(posLettura);
            CheckValue(toCheck, "right");
            posLettura++;
        }

        if(posLettura == LimitArray){
            //arrowMngScript.GetComponent<ArrowMngrScript>().DestorySet();
            //newSet = true;
            
            arrowMngScript.GetComponent<ArrowMngrScript>().DestorySet();

            arrowMngScript.GetComponent<ArrowMngrScript>().SetArrowsArray();
            posLettura = 0;
            roundNumber++;

            if(roundNumber >= ROUNDTOEND)
            {
                float finalResult = p1Points + p2Points;

                //Debug.Log(p1Points);
                //Debug.Log(p2Points);
                //Debug.Log(finalResult);

                if(finalResult > 0)
                {
                    SceneManager.LoadScene(3);
                }
                else
                {
                    SceneManager.LoadScene(4);
                }
            }
        }

        remainingSetText.GetComponent<TMPro.TextMeshProUGUI>().SetText("Remaining sets: " + (ROUNDTOEND - roundNumber));

    }

    public void CheckNewSet(){
        if (newSet)
        {
            arrowMngScript.GetComponent<ArrowMngrScript>().StartCoreGame();
            newSet = false;
        }
        
    }

    private void CheckValue(string toCheck, string ValueFix)
    {
        cdTimer = timerSinglePress;

        if (toCheck == ValueFix)
        {
            bool correct = true;
            ///Call GameAudio from another script to manage the result.
            //Debug.Log("Check OK");
            GameAudio.GetComponent<GameAudio>().SuccessMatch();

            arrowMngScript.GetComponent<ArrowMngrScript>().SwitchBG(correct, posLettura);
            //controllo appartenenza freccia e incremento punteggio di tale giocatore
        }
        else
        {
            bool correct = false;
            //Debug.Log("Error");
            arrowMngScript.GetComponent<ArrowMngrScript>().SwitchBG(correct, posLettura);
            GameAudio.GetComponent<GameAudio>().WrongMatch();
            //controllo appartenenza freccia e decremento punteggio di tale giocatore
        }
    }

    private void Start()
    {
        //Get Array from another script.
        //string[] finalArrow;
        LimitArray = arrowMngScript.GetComponent<ArrowMngrScript>().GetSizeArray();
        timer = Time.time + 0.02f;
        cdTimer = timerSinglePress;
    }

}
