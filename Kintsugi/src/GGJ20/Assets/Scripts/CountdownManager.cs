﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CountdownManager : MonoBehaviour
{
    public AudioClip three;
    public AudioClip two;
    public AudioClip one;
    public AudioClip fight;
    public GameObject img3;
    public GameObject img2;
    public GameObject img1;
    public GameObject imgFight;
    private float timer;
    private AudioSource audioSource;
    private string step;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        step = "3";
    }

    // Update is called once per frame
    void Update()
    {
        if (step.Equals("3"))
        {
            timer = Time.time + 1;
            audioSource.PlayOneShot(three);
            step = "2";
        }

        if(step.Equals("2") && Time.time > timer)
        {
            timer += 1;
            img3.GetComponent<CanvasGroup>().alpha = 0;
            img2.GetComponent<CanvasGroup>().alpha = 1;
            audioSource.PlayOneShot(two);
            step = "1";
        }

        if (step.Equals("1") && Time.time > timer)
        {
            timer += 1;
            img2.GetComponent<CanvasGroup>().alpha = 0;
            img1.GetComponent<CanvasGroup>().alpha = 1;
            audioSource.PlayOneShot(one);
            step = "Fight";
        }

        if (step.Equals("Fight") && Time.time > timer)
        {
            timer += 1;
            img1.GetComponent<CanvasGroup>().alpha = 0;
            imgFight.GetComponent<CanvasGroup>().alpha = 1;
            audioSource.PlayOneShot(fight);
            step = "end";
        }

        if (step.Equals("end"))
        {
            SceneManager.LoadScene(2);
        }
    }
}
