﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ArrowMngrScript : MonoBehaviour
{
    public GameObject gameManager;
    public GameObject[] bgArray;
    public GameObject[] feedbackArray;
    public GameObject[] arrowsArray;
    private GameObject[] ArrowStringSelected = new GameObject[8];
    private GameObject[] BgStringSelected = new GameObject[8];
    public GameObject[] arrowsSpawner;

    public int instanceCounter = 0;

    System.Random randomPick = new System.Random();

    // Start is called before the first frame update
    void Start()
    {
        StartCoreGame();
    }

    ///Chiamare questa funzione per caricare una nuova
    public void StartCoreGame()
    {
        SetArrowsArray();
    }

    public int GetSizeArray(){
        return ArrowStringSelected.Length;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SetArrowsArray()
    {
        for (int i = 0; i < ArrowStringSelected.Length; i++)
        {
            if (arrowsSpawner[i] != null)
            {
                int randArrow = randomPick.Next(arrowsArray.Length);
                int randBG = randomPick.Next(bgArray.Length);
                GameObject arrowToAdd = Instantiate(arrowsArray[randArrow], arrowsSpawner[i].transform);
                GameObject bgToAdd = Instantiate(bgArray[randBG], arrowsSpawner[i].transform);
                
                arrowToAdd.GetComponent<SpriteRenderer>().sortingOrder = 5;
                ArrowStringSelected[i] = arrowToAdd;
                BgStringSelected[i] = bgToAdd;
                
                instanceCounter++;
            }
            else
            {
                break;
            }
        }
    }

    public string GetObj(int pos){
        return ArrowStringSelected[pos].tag.ToString();
    }
    
    public void DestorySet(){
        for (int i = 0; i < ArrowStringSelected.Length; i++)
        {
            if (arrowsSpawner[i] != null)
            {
                Destroy(ArrowStringSelected[i]);
                Destroy(BgStringSelected[i]);
            }
            else
            {
                break;
            }
        }

    }

    public void SwitchBG(bool correct, int pos)
    {

        for (int i = 0; i < bgArray.Length; i++)
        {
            if (correct)
            {
                if (BgStringSelected[pos].name.Equals("Blue(Clone)"))
                {
                    gameManager.GetComponent<CheckInput>().p1Points += 1;
                }
                if (BgStringSelected[pos].name.Equals("Red(Clone)"))
                {
                    gameManager.GetComponent<CheckInput>().p2Points += 1;
                }
                Destroy(BgStringSelected[pos]);
                BgStringSelected[pos]= Instantiate(feedbackArray[0], arrowsSpawner[pos].transform);
            }
            else
            {
                if (BgStringSelected[pos].name.Equals("Blue(Clone)"))
                {
                    gameManager.GetComponent<CheckInput>().p1Points -= 1;
                }
                if (BgStringSelected[pos].name.Equals("Red(Clone)"))
                {
                    gameManager.GetComponent<CheckInput>().p1Points -= 1;
                }
                Destroy(BgStringSelected[pos]);
                BgStringSelected[pos] = Instantiate(feedbackArray[1], arrowsSpawner[pos].transform);
            }
        }
        
    }
    
}
