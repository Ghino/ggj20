﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSession : MonoBehaviour
{
    //public Text field;
    public float targetScore;
    public float currentDisplayScore = 0;


    void Start()
    {
        StartCoroutine(CountUpToTarget());
    }

    IEnumerator CountUpToTarget()
    {
        while (currentDisplayScore < targetScore)
        {
            currentDisplayScore += Time.deltaTime; // or whatever to get the speed you like
            currentDisplayScore = Mathf.Clamp(currentDisplayScore, 0f, targetScore);
            //field.text = currentDisplayScore + "";
            //Debug.Log(currentDisplayScore);
            yield return null;
        }
    }
}
