﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckCombo : MonoBehaviour
{
    public Tracks t;
    //private int indexP1 = 0;
    //private int indexP2 = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.name.Equals("p1"))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                GameObject place = transform.GetChild(i).gameObject;
                place.GetComponent<Image>().sprite = t.p1.images[i];
            }
        }

        if (gameObject.name.Equals("p2"))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                GameObject place = transform.GetChild(i).gameObject;
                place.GetComponent<Image>().sprite = t.p2.images[i];
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.name.Equals("p1"))
        {

        }

        if (gameObject.name.Equals("p2"))
        {

        }
    }
}
