﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Sequence", menuName = "ScriptableObjects/Sequences", order = 1)]
public class Tracks : ScriptableObject
{
    public Track p1;
    public Track p2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

[System.Serializable]
public struct Track
{
    public List<Sprite> images;
    public List<int> values;
}
