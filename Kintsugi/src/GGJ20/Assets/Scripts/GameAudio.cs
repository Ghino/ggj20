﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAudio : MonoBehaviour
{

    [SerializeField] AudioClip WrongAudio;
    [SerializeField] AudioClip SuccesAudio;
    AudioSource AudioSrc;

    // Funzione all'inizio per prelevare il componente AudioSource
    void Start()
    {
        AudioSrc = GetComponent<AudioSource>();
    }

    //Funzione esposta all'esterno per SFX del match sbagliato
    public void WrongMatch(){
        AudioSrc.clip = WrongAudio;
        AudioSrc.Play();
    }

    //Funzione esposta all'esterno per SFX del match giusto
    public void SuccessMatch(){
        AudioSrc.clip = SuccesAudio;
        AudioSrc.Play();
    }

}
