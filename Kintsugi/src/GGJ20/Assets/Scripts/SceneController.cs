﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public GameObject storyToFade;
    public GameObject tutorialText;
    public GameObject titleScreen;
    //public int secondsToWait = 3;
    public int secondsToFade = 3;
    private float timer;
    private bool tutorial = false;
    private bool story = false;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) && !story && !tutorial)
        {
            story = true;
            titleScreen.GetComponent<CanvasGroup>().alpha = 0;
            storyToFade.GetComponent<CanvasGroup>().alpha = 1;
            timer = Time.time + secondsToFade;
        }

        if (!tutorial && story && (timer - Time.time) <= secondsToFade)
        {
            float value = (timer - Time.time) / secondsToFade;
            storyToFade.GetComponent<CanvasGroup>().alpha = value;
            if (storyToFade.GetComponent<CanvasGroup>().alpha <= 0)
            {
                tutorialText.GetComponent<CanvasGroup>().alpha = 1;
                tutorial = true;
            }
        }

        //if(Time.time <= timer)
        //{
        //    if (Input.GetKeyDown(KeyCode.Space))
        //    {
        //        story = true;
        //        storyToFade.GetComponent<CanvasGroup>().alpha = 0;
        //        titleScreen.GetComponent<CanvasGroup>().alpha = 1;
        //        gameObject.GetComponent<AudioSource>().Play();
        //    }
        //    if (!story && (timer - Time.time) <= secondsToFade)
        //    {
        //        float value = (timer - Time.time) / secondsToFade;
        //        storyToFade.GetComponent<CanvasGroup>().alpha = value;
        //    }
        //} else if(!story)
        //{
        //    story = true;
        //    titleScreen.GetComponent<CanvasGroup>().alpha = 1;
        //    gameObject.GetComponent<AudioSource>().Play();
        //}

        //if (story && Input.GetKeyDown(KeyCode.Return))
        //{
        //    if (!tutorial)
        //    {
        //        titleScreen.GetComponent<CanvasGroup>().alpha = 0;
        //        tutorialText.GetComponent<CanvasGroup>().alpha = 1;
        //        //timer += secondsToWait;
        //        tutorial = true;
        //    }
        //}

        if (tutorial && Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene(1);
        }
    }
}
