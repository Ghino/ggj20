﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckInput : MonoBehaviour
{

    ///ERROR ON COMMIT
    ///Function del singolo IF
    ///(OK)->Prendere in input la freccia premuta
    ///(OK)->Inserirlo in un array per eseguire calcoli di statistica
    ///(OK)->Aumentare il puntatore -> Se è arrivato al limite uscire dalla funzione
    ///(OK)->Prelevare il GameObject delle freccie -> confrontare se è uguale al valore inserito

    //[SerializeField] GameObject[] arrayRow;
    [SerializeField] GameObject arrowMngScript;
    [SerializeField] GameObject GameAudio;
    [SerializeField] bool newSet;

    public GameObject remainingSetText;

    //[SerializeField] GameObject PS; ///To implement, not mandatory.
    public List<string> finalArrow;
    int posLettura = 0;
    int LimitArray;

    //Check a ogni frame dell'input inserito.
    public void Update()
    {
        if (Input.GetKeyDown("up"))
        {
            //Debug.Log("Up pressed");
            finalArrow.Add("up");
            //string toCheck = arrayRow[posLettura].GetComponent<ArrowScript>().GetValue(); //Use GetFunction to get value of arrow. 
            ///Ottenere valore della freccia nella posizione posLettura
            string toCheck = arrowMngScript.GetComponent<ArrowMngrScript>().GetObj(posLettura);
            CheckValue(toCheck, "up");
            posLettura++;
        }
        else if (Input.GetKeyDown("down")){
            Debug.Log("Down pressed");
            finalArrow.Add("down");
            //string toCheck = arrayRow[posLettura].GetComponent<ArrowScript>().GetValue(); //Use GetFunction to get value of arrow. 
            //CheckValue(toCheck, "down");
            string toCheck = arrowMngScript.GetComponent<ArrowMngrScript>().GetObj(posLettura);
            CheckValue(toCheck, "down");
            posLettura++;
        }
        else if (Input.GetKeyDown("left")){
            Debug.Log("Left pressed");
            finalArrow.Add("left");
            //string toCheck = arrayRow[posLettura].GetComponent<ArrowScript>().GetValue(); //Use GetFunction to get value of arrow. 
            //CheckValue(toCheck, "left");
            string toCheck = arrowMngScript.GetComponent<ArrowMngrScript>().GetObj(posLettura);
            CheckValue(toCheck, "left");
            posLettura++;
        }
        else if (Input.GetKeyDown("right")){
            Debug.Log("Right pressed");
            finalArrow.Add("right");
            //string toCheck = arrayRow[posLettura].GetComponent<ArrowScript>().GetValue(); //Use GetFunction to get value of arrow. 
            //CheckValue(toCheck, "right");
            string toCheck = arrowMngScript.GetComponent<ArrowMngrScript>().GetObj(posLettura);
            CheckValue(toCheck, "right");
            posLettura++;
        }

        if(posLettura == LimitArray){
            //arrowMngScript.GetComponent<ArrowMngrScript>().DestorySet();
            //newSet = true;
            posLettura = 0;
            arrowMngScript.GetComponent<ArrowMngrScript>().DestorySet();
            arrowMngScript.GetComponent<ArrowMngrScript>().SetArrowsArray();
        }

        remainingSetText.GetComponent<TMPro.TextMeshProUGUI>().SetText("Set rimanenti: ");

    }

    public void CheckNewSet(){
        if (newSet)
        {
            arrowMngScript.GetComponent<ArrowMngrScript>().StartCoreGame();
            newSet = false;
        }
        
    }

    private void CheckValue(string toCheck, string ValueFix)
    {
        if (toCheck == ValueFix)
        {
            ///Call GameAudio from another script to manage the result.
            //Debug.Log("Check OK");
            GameAudio.GetComponent<GameAudio>().SuccessMatch();
            //To do -> Implement the PS trigger on Success check
        }
        else
        {
            Debug.Log("Error");
            GameAudio.GetComponent<GameAudio>().WrongMatch(); 
        }
    }

    private void Start()
    {
        //Get Array from another script.
        //string[] finalArrow;
        LimitArray = arrowMngScript.GetComponent<ArrowMngrScript>().GetSizeArray();
    }

}
